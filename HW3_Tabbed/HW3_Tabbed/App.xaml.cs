﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Diagnostics;

using Xamarin.Forms;

//[assembly: XamlCompilation(XamlCompilationOptions.Compile)]
namespace HW3_Tabbed
{
	public partial class App : Application
	{
		public App ()
		{
			InitializeComponent();
            Debug.WriteLine($"**** {this.GetType().Name}.{nameof(App)}:  ctor");
            MainPage = new NaviTab();
		}

		protected override void OnStart ()
		{
			// Handle when your app starts
		}

		protected override void OnSleep ()
		{
			// Handle when your app sleeps
		}

		protected override void OnResume ()
		{
			// Handle when your app resumes
		}
	}
}
